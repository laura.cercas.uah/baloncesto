<%@ page import="java.util.ArrayList"%>
<%@ page import="entities.Jugador"%>
<!DOCTYPE >
<html lang="es">
    <head>
        <title>Votaci&oacute;n mejor jugador liga ACB</title>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
        <style>
            .table {
                border: 2px solid #dddddd;
                margin: 20px;
                border-spacing: 25px;
                width: 500px;
            }
            .fila{
                text-align:center;
            }
        </style>
    </head>
    <body class="resultado">
        <p style="font-size: 40;">
            Votaci&oacute;n al mejor jugador de la liga ACB
        <hr/>
        <h2>Resultados de la votaci&oacute;n:</h2>
        <table class="table" id="tablaVotos">
            <th class="fila"><b>Jugador</b></th>
            <th class="fila"><b>N&uacute;mero de votos</b></th>
          <% ArrayList<Jugador> jugadores = (ArrayList<Jugador>) session.getAttribute("jugadores");
             for(Jugador j: jugadores){ 
            %> 
                <tr> 
                    <td class="fila"><%=j.getNombre()%></td>
                    <td class="fila"><%=j.getVotos()%></td>
                </tr>
              <%}%>
          </table>
         <br />
        <br> <a href="index.html"> Ir al comienzo</a>
    </body>
</html>