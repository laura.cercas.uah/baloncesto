
import java.io.*;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.*;

import entities.Jugador;

public class Acb extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        String action = req.getParameter("action");


        if (action.equalsIgnoreCase("votar")) {
            String nombreP = req.getParameter("txtNombre");
            String nombre = req.getParameter("R1");
            if (nombre.equals("Otros")) {
                nombre = req.getParameter("txtOtros");
            }
            if (bd.existeJugador(nombre)) {
                bd.actualizarJugador(nombre);
            } else {
                bd.insertarJugador(nombre);
            }
            s.setAttribute("nombreCliente", nombreP);

            res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
        }

        if (action.equalsIgnoreCase("votosCero")) { 
            bd.votosACero();
            res.sendRedirect(res.encodeRedirectURL("votosCero.jsp"));
        }

        if (action.equalsIgnoreCase("verVotos")) { 
            ArrayList<Jugador> jugadores = bd.verVotosJugadores();
            s.setAttribute("jugadores", jugadores);
            res.sendRedirect(res.encodeRedirectURL("verVotos.jsp"));
        }

    }

    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
