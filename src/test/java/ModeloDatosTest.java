import org.junit.jupiter.api.Test;

import entities.Jugador;

import static org.junit.jupiter.api.Assertions.*;

public class ModeloDatosTest {
    @Test
    public void testExisteJugador() {
        System.out.println("Prueba de existeJugador");
        String nombre = "";
        ModeloDatos instance = new ModeloDatos();
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
    }

    @Test
    public void testActualizarJugador() {
        System.out.println("Prueba de actualizarJugador");
        String nombre = "Rudy";
        ModeloDatos instance = new ModeloDatos();
        Jugador jugadorInit = instance.verJugador(nombre);
        int expectedResult = jugadorInit.getVotos()+1;

        instance.actualizarJugador(nombre);
        Jugador jugador = instance.verJugador(nombre);

        int result = jugador.getVotos();

        assertEquals(expectedResult, result);
    }
}