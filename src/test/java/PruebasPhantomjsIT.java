import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

public class PruebasPhantomjsIT {
    private static WebDriver driver = null;

    @Test
    public void tituloIndexTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    public void setVotosACeroTest(){
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        //Pulsamos boton de poner los votos a cero y volvemos a la pantalla principal
        driver.findElement(By.id("votosCero")).click();
        driver.findElement(By.id("volverIndex")).click();

        //Pulsamos boton Ver votos
        driver.findElement(By.id("verVotos")).click();
        
        //Cogemos los valores de la tabla de votos y jugadores
        WebElement tablaJugadores = driver.findElement(By.id("tablaVotos"));
        ArrayList<WebElement> filas = new ArrayList<>(tablaJugadores.findElements(By.tagName("tr")));
        //Eliminamos la fila de cabeceras
        filas.remove(0);

        ArrayList<WebElement> listaJugadores = new ArrayList<>();
        for(WebElement f : filas){
            ArrayList<WebElement> celdas = (ArrayList<WebElement>) f.findElements(By.tagName("td"));
            //Añadimos a la lista solo el valor de los votos del jugador
            listaJugadores.add(celdas.get(1));
        }

        int expectedResult = 0;
        int counter = 0;
        //Se van sumando los votos de los jugadores tras pulsar el votos de poner votos a cero
        for(WebElement j : listaJugadores){
            counter +=Integer.parseInt(j.getText().trim());
        }
        assertEquals(expectedResult, counter,"No se han reseteado todos los votos");
    }

    @Test
    public void votarOtroJugadorTest(){
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        //Pulsar radio Otro, introducir un nombre de jugador y dar a votar
        driver.findElement(By.id("otroRadio")).click();
        String jugador = "Ricky Rubio";
        driver.findElement(By.id("otroText")).sendKeys(jugador);
        driver.findElement(By.id("votar")).click();

        //Volver a pantalla principal
        driver.findElement(By.id("volverIndex")).click();

        driver.findElement(By.id("verVotos")).click();
        
        //Cogemos los valores de la tabla de votos y jugadores
        WebElement tablaJugadores = driver.findElement(By.id("tablaVotos"));
        ArrayList<WebElement> filas = new ArrayList<>(tablaJugadores.findElements(By.tagName("tr")));
        //Eliminamos la fila de cabeceras
        filas.remove(0);

        //Separamos los valores de la columna de Jugadores y columna Votos
        ArrayList<WebElement> listaJugadores = new ArrayList<>();
        ArrayList<WebElement> listaVotos = new ArrayList<>();
        for(WebElement f : filas){
            ArrayList<WebElement> celdas = (ArrayList<WebElement>) f.findElements(By.tagName("td"));
            listaJugadores.add(celdas.get(0));
            listaVotos.add(celdas.get(1));
        }

        int expectedResult = 1;
        int counter = 0;
        //Comprobamos que el voto para el jugador con el nombre que hemos introducidos es 1
        for(int i = 0; i < filas.size(); i++){
            if(jugador.trim().equals(listaJugadores.get(i).getText().trim())){
                counter +=Integer.parseInt(listaVotos.get(i).getText().trim());
            }
        }
        assertEquals(expectedResult, counter,"No se ha registrado el voto del jugador");
    }
}
